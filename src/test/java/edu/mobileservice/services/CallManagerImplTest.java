package edu.mobileservice.services;

import edu.mobileservice.dao.implementations.CallDAOImpl;
import edu.mobileservice.model.CallEntity;
import edu.mobileservice.model.UserEntity;
import edu.mobileservice.services.implementations.CallManagerImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static edu.mobileservice.common.Constants.ZERO;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class CallManagerImplTest {

    private static String RECEIVER_NUMBER_STUB = "Number STUB";
    private static Integer EXISTING_CALL_ID = 100;
    private static Integer NOT_EXISTING_CALL_ID = 200;

    @Mock
    private CallDAOImpl callDAO;
    @InjectMocks
    private CallManagerImpl testedUnit;

    @Test
    public void findAllCallsTest() {
        final CallEntity callEntity = new CallEntity();
        List<CallEntity> expectedResult = Arrays.asList(callEntity);
        when(callDAO.findAll()).thenReturn(expectedResult);

        final List<CallEntity> actualResult = testedUnit.findAllCalls();

        Assert.assertEquals(expectedResult.size(), actualResult.size());
        Assert.assertEquals(expectedResult.get(ZERO), actualResult.get(ZERO));
    }

    @Test
    public void findUsersByReceiverNumberTest() {
        final UserEntity userEntity = new UserEntity();
        List<UserEntity> expectedResult = Arrays.asList(userEntity);
        when(callDAO.findUsersByReceiverNumber(RECEIVER_NUMBER_STUB)).thenReturn(expectedResult);

        final List<UserEntity> actualResult = testedUnit.findUsersByReceiverNumber(RECEIVER_NUMBER_STUB);

        Assert.assertEquals(expectedResult.size(), actualResult.size());
        Assert.assertEquals(expectedResult.get(ZERO), actualResult.get(ZERO));
    }

    @Test
    public void findCallByIdTestWhenExistingId() {
        final CallEntity expectedResult = new CallEntity();
        when(callDAO.findByID(EXISTING_CALL_ID)).thenReturn(expectedResult);

        final CallEntity actualResult = testedUnit.findCallById(EXISTING_CALL_ID);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test(expected = IllegalStateException.class)
    public void findCallByIdTestWhenNotExistingId() {
        when(callDAO.findByID(NOT_EXISTING_CALL_ID)).thenReturn(null);

        testedUnit.findCallById(NOT_EXISTING_CALL_ID);
    }
}
