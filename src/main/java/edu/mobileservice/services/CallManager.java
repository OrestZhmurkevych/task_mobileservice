package edu.mobileservice.services;

import edu.mobileservice.model.CallEntity;
import edu.mobileservice.model.UserEntity;

import java.util.List;


public interface CallManager {

    List<CallEntity> findAllCalls();

    List<UserEntity> findUsersByReceiverNumber(final String receiverNumber);

    CallEntity findCallById(final Integer id);

    boolean create(CallEntity call);

    boolean update(CallEntity call);

    boolean deleteCall(CallEntity call);
}
