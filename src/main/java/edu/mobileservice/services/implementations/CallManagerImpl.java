package edu.mobileservice.services.implementations;

import edu.mobileservice.dao.implementations.CallDAOImpl;
import edu.mobileservice.model.CallEntity;
import edu.mobileservice.model.UserEntity;
import edu.mobileservice.services.CallManager;

import java.util.List;
import java.util.Objects;


public class CallManagerImpl implements CallManager {

    private CallDAOImpl callDAO = new CallDAOImpl();

    @Override
    public List<CallEntity> findAllCalls() {
        return callDAO.findAll();
    }

    @Override
    public List<UserEntity> findUsersByReceiverNumber(final String receiverNumber) {
        return callDAO.findUsersByReceiverNumber(receiverNumber);
    }

    @Override
    public CallEntity findCallById(final Integer id) {
        final CallEntity callEntity = callDAO.findByID(id);
        if(Objects.isNull(callEntity))
        {
            throw new IllegalStateException(String.format("Call with %s id not exists", id));
        }
        return callEntity;
    }

    @Override
    public boolean create(CallEntity call) {
        if(Objects.isNull(call))
        {
            throw new IllegalStateException(String.format("Call doesn't exist"));
        }
        return callDAO.create(call);
    }

    @Override
    public boolean update(CallEntity call) {
        if(Objects.isNull(call))
        {
            throw new IllegalStateException(String.format("Call doesn't exist"));
        }
        return callDAO.update(call);
    }

    @Override
    public boolean deleteCall(CallEntity call) {
        if(Objects.isNull(call))
        {
            throw new IllegalStateException(String.format("Call doesn't exist"));
        }
        return callDAO.delete(call.getId());
    }
}
