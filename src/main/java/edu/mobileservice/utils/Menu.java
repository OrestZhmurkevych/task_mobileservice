package edu.mobileservice.utils;

import edu.mobileservice.model.*;
import edu.mobileservice.services.*;
import edu.mobileservice.services.implementations.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;


public class Menu {

    private static Logger logger = LogManager.getLogger("Menu");
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private UsersManager usersManager = new UsersManagerImpl();
    private MobileNumberManager mobileNumberManager = new MobileNumberManagerImpl();
    private CallManager callManager = new CallManagerImpl();
    private SMSManager smsManager = new SMSManagerImpl();
    private PricePlaneManager pricePlanManager = new PricePlanManagerImpl();
    private UsersHaveMobileDevicesManager usersHaveMobileDevicesManager = new UsersHaveMobileDevicesManagerImpl();
    private MobileDeviceManager mobileDeviceManager = new MobileDeviceManagerImpl();

    public void show() {
        logger.info("Choose the action");
        logger.info("Select the table to interact - press 1");
        logger.info("Find users of network by receiver number(based on 3-table JOIN) - press 2");
        logger.info("Find devices by owner ID(based on 3-table JOIN) - press 3");
        logger.info("Enter 4 to exit");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    selectTable();
                    break;
                case 2:
                    logger.info("Enter the receiver number for search");
                    String receiverNumberForSearch = br.readLine();
                    logger.info(callManager.findUsersByReceiverNumber(receiverNumberForSearch));
                    break;
                case 3:
                    logger.info("Enter the owner id for search");
                    Integer ownerIdForSearch = Integer.valueOf(br.readLine());
                    logger.info(usersManager.findMobileDevicesByOwner(ownerIdForSearch));
                    break;
                case 4:
                    break;
            }
        } catch (IOException e) {
            logger.error("Menu show error: " + e.getMessage());
        }
    }

    public void selectTable() {
        logger.info("Select the table");
        logger.info("Table users - press 1");
        logger.info("Table mobile_number - press 2");
        logger.info("Table calls - press 3");
        logger.info("Table sms - press 4");
        logger.info("Table price_plan - press 5");
        logger.info("Table users_have_mobile_devices - press 6");
        logger.info("Table mobile_devices - press 7");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    selectTableUsersAction();
                    break;
                case 2:
                    selectTableMobileNumberAction();
                    break;
                case 3:
                    selectTableCallsAction();
                    break;
                case 4:
                    selectTableSMSAction();
                    break;
                case 5:
                    selectTablePricePlanAction();
                    break;
                case 6:
                    selectTableUsersHaveMobileDevicesAction();
                    break;
                case 7:
                    selectTableMobileDeviceAction();
                    break;
            }
        } catch (IOException e) {
            logger.error("Menu select table error: " + e.getMessage());
        }
    }

    public void selectTableUsersAction() {
        logger.info("Table users");
        logger.info("Find all - press 1");
        logger.info("Find user by id - press 2");
        logger.info("Create user - press 3");
        logger.info("Update user - press 4");
        logger.info("Delete user - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(usersManager.findAllUsers());
                    break;
                case 2:
                    logger.info("Enter id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(usersManager.findUser(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for user you want to create");
                    UserEntity userCreateEntity = getParametersAndCreateUserEntityWithThem();
                    final boolean createResult = usersManager.create(userCreateEntity);
                    if (createResult) {
                        logger.info("User added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for user you want to update");
                    UserEntity userUpdateEntity = getParametersAndCreateUserEntityWithThem();
                    final boolean updateResult = usersManager.update(userUpdateEntity);
                    if (updateResult) {
                        logger.info("User updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters for user you want to delete");
                    UserEntity userDeleteEntity = getParametersAndCreateUserEntityWithThem();
                    final boolean deleteResult = usersManager.deleteUser(userDeleteEntity);
                    if (deleteResult) {
                        logger.info("User deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table users error: " + e.getMessage());
        }
    }

    private UserEntity getParametersAndCreateUserEntityWithThem() throws IOException {
        logger.info("Id:");
        Integer createId = Integer.valueOf(br.readLine());
        logger.info("First name:");
        String createFirstName = br.readLine();
        logger.info("Surname:");
        String createSurname = br.readLine();
        return new UserEntity(createId, createFirstName, createSurname);
    }

    public void selectTableMobileNumberAction() {
        logger.info("Table mobile_number");
        logger.info("Find all - press 1");
        logger.info("Find mobile number by id - press 2");
        logger.info("Create mobile number - press 3");
        logger.info("Update mobile number - press 4");
        logger.info("Delete mobile number - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(mobileNumberManager.findAllMobileNumbers());
                    break;
                case 2:
                    logger.info("Enter id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(mobileNumberManager.findMobileNumber(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for user you want to create");
                    MobileNumberEntity mobileNumberCreateEntity = getParametersAndCreateMobileNumberEntityWithThem();
                    final boolean createResult = mobileNumberManager.create(mobileNumberCreateEntity);
                    if (createResult) {
                        logger.info("Mobile number added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for user you want to update");
                    MobileNumberEntity mobileNumberUpdateEntity = getParametersAndCreateMobileNumberEntityWithThem();
                    final boolean updateResult = mobileNumberManager.update(mobileNumberUpdateEntity);
                    if (updateResult) {
                        logger.info("Mobile number updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters for user you want to delete");
                    MobileNumberEntity mobileNumberDeleteEntity = getParametersAndCreateMobileNumberEntityWithThem();
                    final boolean deleteResult = mobileNumberManager.deleteMobileNumber(mobileNumberDeleteEntity);
                    if (deleteResult) {
                        logger.info("Mobile number deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table mobile_number error: " + e.getMessage());
        }
    }

    private MobileNumberEntity getParametersAndCreateMobileNumberEntityWithThem() throws IOException {
        logger.info("Id:");
        Integer createId = Integer.valueOf(br.readLine());
        logger.info("Number:");
        String createNumber = br.readLine();
        logger.info("Price plan Id:");
        Integer createPricePlanId = Integer.valueOf(br.readLine());
        logger.info("User Id:");
        Integer createUserId = Integer.valueOf(br.readLine());
        return new MobileNumberEntity(createId, createNumber, createPricePlanId, createUserId);
    }

    public void selectTableCallsAction() {
        logger.info("Table calls");
        logger.info("v all - press 1");
        logger.info("Find call by id - press 2");
        logger.info("Create call - press 3");
        logger.info("Update call - press 4");
        logger.info("Delete call - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(callManager.findAllCalls());
                    break;
                case 2:
                    logger.info("Enter id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(callManager.findCallById(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for call you want to create");
                    CallEntity callCreateEntity = getParametersAndCreateCallEntityWithThem();
                    final boolean createResult = callManager.create(callCreateEntity);
                    if (createResult) {
                        logger.info("Call added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for call you want to update");
                    CallEntity callUpdateEntity = getParametersAndCreateCallEntityWithThem();
                    final boolean updateResult = callManager.update(callUpdateEntity);
                    if (updateResult) {
                        logger.info("Call updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters for call you want to delete");
                    CallEntity callDeleteEntity = getParametersAndCreateCallEntityWithThem();
                    final boolean deleteResult = callManager.deleteCall(callDeleteEntity);
                    if (deleteResult) {
                        logger.info("Call deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table calls error: " + e.getMessage());
        }
    }

    private CallEntity getParametersAndCreateCallEntityWithThem() throws IOException {
        logger.info("Id:");
        Integer createId = Integer.valueOf(br.readLine());
        logger.info("Caller number Id:");
        Integer createCallerNumberId = Integer.valueOf(br.readLine());
        logger.info("Receiver number:");
        String createReceiverNumber = br.readLine();
        logger.info("Duration:");
        Integer createDuration = Integer.valueOf(br.readLine());
        logger.info("Type:");
        String createType = br.readLine();
        return new CallEntity(createId, createCallerNumberId, createReceiverNumber, createDuration, createType);
    }

    public void selectTableSMSAction() {
        logger.info("Table sms");
        logger.info("Find all - press 1");
        logger.info("Find sms by id - press 2");
        logger.info("Create sms - press 3");
        logger.info("Update sms - press 4");
        logger.info("Delete sms - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(smsManager.findAllSMS());
                    break;
                case 2:
                    logger.info("Enter id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(smsManager.findSMSById(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for sms you want to create");
                    SMSEntity smsCreateEntity = getParametersAndCreateSMSEntityWithThem();
                    final boolean createResult = smsManager.create(smsCreateEntity);
                    if (createResult) {
                        logger.info("SMS added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for sms you want to update");
                    SMSEntity smsUpdateEntity = getParametersAndCreateSMSEntityWithThem();
                    final boolean updateResult = smsManager.update(smsUpdateEntity);
                    if (updateResult) {
                        logger.info("SMS updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters for sms you want to delete");
                    SMSEntity smsDeleteEntity = getParametersAndCreateSMSEntityWithThem();
                    final boolean deleteResult = smsManager.deleteSMS(smsDeleteEntity);
                    if (deleteResult) {
                        logger.info("SMS deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table sms error: " + e.getMessage());
        }
    }

    private SMSEntity getParametersAndCreateSMSEntityWithThem() throws IOException {
        logger.info("Id:");
        Integer createId = Integer.valueOf(br.readLine());
        logger.info("Sender number Id:");
        Integer createSenderNumberId = Integer.valueOf(br.readLine());
        logger.info("Receiver number:");
        String createReceiverNumber = br.readLine();
        logger.info("Text:");
        String createText = br.readLine();
        return new SMSEntity(createId, createSenderNumberId, createReceiverNumber, createText);
    }

    public void selectTablePricePlanAction() {
        logger.info("Table price_plan");
        logger.info("Find all - press 1");
        logger.info("Find price plan by id - press 2");
        logger.info("Create price plan - press 3");
        logger.info("Update price plan - press 4");
        logger.info("Delete price plan - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(pricePlanManager.findAllPricePlans());
                    break;
                case 2:
                    logger.info("Enter id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(pricePlanManager.findPricePlanById(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for price plan you want to create");
                    PricePlanEntity pricePlanCreateEntity = getParametersAndCreatePricePlanEntityWithThem();
                    final boolean createResult = pricePlanManager.create(pricePlanCreateEntity);
                    if (createResult) {
                        logger.info("Price plan added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for price plan you want to update");
                    PricePlanEntity pricePlanUpdateEntity = getParametersAndCreatePricePlanEntityWithThem();
                    final boolean updateResult = pricePlanManager.update(pricePlanUpdateEntity);
                    if (updateResult) {
                        logger.info("Price plan updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters for price plan you want to delete");
                    PricePlanEntity pricePlanDeleteEntity = getParametersAndCreatePricePlanEntityWithThem();
                    final boolean deleteResult = pricePlanManager.deletePricePlan(pricePlanDeleteEntity);
                    if (deleteResult) {
                        logger.info("Price plan deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table price_plan error: " + e.getMessage());
        }
    }

    private PricePlanEntity getParametersAndCreatePricePlanEntityWithThem() throws IOException {
        logger.info("Id:");
        Integer createId = Integer.valueOf(br.readLine());
        logger.info("Name:");
        String createName = br.readLine();
        logger.info("Specifications:");
        String createSpecifications = br.readLine();
        logger.info("Price:");
        BigDecimal createPrice = new BigDecimal(br.readLine());
        logger.info("Mobile number Id:");
        Integer createMobileNumberId = Integer.valueOf(br.readLine());
        return new PricePlanEntity(createId, createName, createSpecifications, createPrice, createMobileNumberId);
    }

    public void selectTableUsersHaveMobileDevicesAction() {
        logger.info("Table price_plan");
        logger.info("Find all - press 1");
        logger.info("Find users with mobile devices by user id - press 2");
        logger.info("Create user with mobile device - press 3");
        logger.info("Update user with mobile device - press 4");
        logger.info("Delete user with mobile device - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(usersHaveMobileDevicesManager.findAllUsersHaveMobileDevices());
                    break;
                case 2:
                    logger.info("Enter user id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(usersHaveMobileDevicesManager.findUsersHaveMobileDevicesByUserId(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for user having mobile device you want to create");
                    UsersHaveMobileDevicesEntity usersHaveMobileDevicesCreateEntity =
                            getParametersAndCreateUsersHaveMobileDevicesEntityWithThem();
                    final boolean createResult = usersHaveMobileDevicesManager.create(usersHaveMobileDevicesCreateEntity);
                    if (createResult) {
                        logger.info("User with mobile device added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for user having mobile device you want to update");
                    UsersHaveMobileDevicesEntity usersHaveMobileDevicesUpdateEntity =
                            getParametersAndCreateUsersHaveMobileDevicesEntityWithThem();
                    final boolean updateResult = usersHaveMobileDevicesManager.update(usersHaveMobileDevicesUpdateEntity);
                    if (updateResult) {
                        logger.info("User with mobile device updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters for user having mobile device you want to delete");
                    UsersHaveMobileDevicesEntity usersHaveMobileDevicesDeleteEntity =
                            getParametersAndCreateUsersHaveMobileDevicesEntityWithThem();
                    final boolean deleteResult = usersHaveMobileDevicesManager.
                            deleteUsersHaveMobileDevices(usersHaveMobileDevicesDeleteEntity);
                    if (deleteResult) {
                        logger.info("User with mobile device deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table users_have_mobile_devices error: " + e.getMessage());
        }
    }

    private UsersHaveMobileDevicesEntity getParametersAndCreateUsersHaveMobileDevicesEntityWithThem() throws IOException {
        logger.info("User Id:");
        Integer createUserId = Integer.valueOf(br.readLine());
        logger.info("Mobile device Id:");
        Integer createMobileDeviceId = Integer.valueOf(br.readLine());
        return new UsersHaveMobileDevicesEntity(createUserId, createMobileDeviceId);
    }

    public void selectTableMobileDeviceAction() {
        logger.info("Table price_plan");
        logger.info("Find all - press 1");
        logger.info("Find mobile device by id - press 2");
        logger.info("Create mobile device - press 3");
        logger.info("Update mobile device - press 4");
        logger.info("Delete mobile device - press 5");
        try {
            int choice = Integer.valueOf(br.readLine());
            switch (choice) {
                case 1:
                    logger.info(mobileDeviceManager.findAllMobileDevices());
                    break;
                case 2:
                    logger.info("Enter mobile device id for search");
                    int searchId = Integer.valueOf(br.readLine());
                    logger.info(mobileDeviceManager.findMobileDeviceById(searchId));
                    break;
                case 3:
                    logger.info("Enter the parameters for mobile device you want to create");
                    MobileDeviceEntity mobileDeviceCreateEntity = getParametersAndCreateMobileDeviceEntityWithThem();
                    final boolean createResult = mobileDeviceManager.create(mobileDeviceCreateEntity);
                    if (createResult) {
                        logger.info("Mobile device added");
                    }
                    break;
                case 4:
                    logger.info("Enter the parameters for mobile device you want to update");
                    MobileDeviceEntity mobileDeviceUpdateEntity = getParametersAndCreateMobileDeviceEntityWithThem();
                    final boolean updateResult = mobileDeviceManager.update(mobileDeviceUpdateEntity);
                    if (updateResult) {
                        logger.info("Mobile device updated");
                    }
                    break;
                case 5:
                    logger.info("Enter the parameters mobile device you want to delete");
                    MobileDeviceEntity mobileDeviceDeleteEntity = getParametersAndCreateMobileDeviceEntityWithThem();
                    final boolean deleteResult = mobileDeviceManager.delete(mobileDeviceDeleteEntity);
                    if (deleteResult) {
                        logger.info("Mobile device deleted");
                    }
                    break;
            }
            logger.info("Going back to menu\n\n");
            show();
        } catch (IOException e) {
            logger.error("Table mobile_devices error: " + e.getMessage());
        }
    }

    private MobileDeviceEntity getParametersAndCreateMobileDeviceEntityWithThem() throws IOException {
        logger.info("Id:");
        Integer createId = Integer.valueOf(br.readLine());
        logger.info("Brand:");
        String createBrand = br.readLine();
        logger.info("Model:");
        String createModel = br.readLine();
        return new MobileDeviceEntity(createId, createBrand, createModel);
    }
}
